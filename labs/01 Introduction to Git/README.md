# My Git Notes v1.2

Detailed notes showing how to use Git.

## 2.6 Committing Changes

Perform the following steps

1. git add README.md -> Add to staging area
2. git commit -m 'Descriptive message.' -> commit changes

Final changes.