
var request = new XMLHttpRequest();
request.open('GET', 'data/books.json', false);
request.send(null);
var data = JSON.parse(request.responseText);
console.log(data);

var books = data.books;
var titles = ['Title','Year','Author','ISBN'];

var table = document.createElement('table');
var tr = document.createElement('tr');
for (var i = 0; i<titles.length; i++) {
	var td = document.createElement('td');
	var text = document.createTextNode(titles[i]);
	td.appendChild(text);
	tr.appendChild(td);
	table.appendChild(tr);
}

for (var i = 0; i < books.length; i++) {
    var tr = document.createElement('tr');   

    var td1 = document.createElement('td');
    var td2 = document.createElement('td');
    var td3 = document.createElement('td');
    var td4 = document.createElement('td');

    var text1 = document.createTextNode(books[i].title);
    var text2 = document.createTextNode(books[i].year);
    var text3 = document.createTextNode(books[i].authors);
    var text4 = document.createTextNode(books[i].isbn);

    td1.appendChild(text1);
    td2.appendChild(text2);
    td3.appendChild(text3);
    td4.appendChild(text4);
    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);
    tr.appendChild(td4);
    tr.onclick = function(){
    	var row = tr.rowIndex;
    	var title = books[row].title;
    	document.querySelector('#book p').innerHTML = 'Selected book: '+ title};
	
    table.appendChild(tr);
    
}

document.body.appendChild(table);
